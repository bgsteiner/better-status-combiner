-- Control aliases
Status = Controls.Status
LedOK = Controls.Leds[1]
LedComp = Controls.Leds[2]
LedFault = Controls.Leds[3]
LedPresent = Controls.Leds[4]
LedMissing = Controls.Leds[5]
LedInit = Controls.Leds[6]

List = Controls.List
JSONOut = Controls.JSON

--Inputs
DetailLevel = Controls.Detail
Filter = Controls.Filter

Inputs = Controls.Input
BtnDisable = Controls.Disable
BtnSuppress = Controls.Suppress
BtnLatch = Controls.Latch
BtnReset = Controls.Reset

Labels = Controls.Label

function protect(tbl)
  return setmetatable({}, {
      __index = tbl,
      __newindex = function(t, key, value)
          error("attempting to change constant " ..
                 tostring(key) .. " to " .. tostring(value), 2)
      end
  })
end

function table.clone(orig)
  local orig_type = type(orig)
  local copy = {}
  if orig_type == 'table' then
      copy = {}
      for orig_key, orig_value in next, orig, nil do
          copy[table.clone(orig_key)] = table.clone(orig_value)
      end
      setmetatable(copy, table.clone(getmetatable(orig)))
  else -- number, string, boolean, etc
      copy = orig
  end
  return copy
end

-- Constants
rapidjson = require("rapidjson")

constants = {
  Qty = Properties["Input Count"].Value,
  States = {OK=0, COMPROMISED=1, FAULT=2, NOTPRESENT=3, MISSING=4, INITIALIZING=5},
  StateText = {"OK", "COMPROMISED", "FAULT", "NOTPRESENT", "MISSING", "INITIALIZING"},
  LEDs = {true, false, false, false, false, false}
}
const = {}
const.Qty = constants.Qty
const.States = protect(constants.States)
const.StateText = protect(constants.StateText)
const.LEDs = protect(constants.LEDs)
const = protect(constants)

--Variables
inputStates = {}
statusJson = {}

--Event Handlers

function filterSelect(e)
  local a = 0
  if e == 7 then for i=1,6 do Filter[i].Boolean = true Filter[8].Boolean = false end
  elseif e == 8 then for i=1,6 do Filter[i].Boolean = false Filter[7].Boolean = false end
  else for i=1,6 do if Filter[i].Boolean then a = a+1 end end Filter[7].Boolean=a==6 Filter[8].Boolean=a==0 end
  updateDisplay()
end

function setLED(val)
  val = val or const.LEDs
  for i=1,#val do
    Controls.Leds[i].Boolean = val[i]
  end
end

function setStatus(level)
  local text = {}
  local statusText = ""

  if DetailLevel.String ~= "None" and level ~=0 then
    for i=1, const.Qty do
      local s = inputStates[i].Status
      local t = inputStates[i].String
      local n = inputStates[i].Name

      if s == level then
        local st = ""
        if DetailLevel.String == "Label" then
          st = n
        elseif DetailLevel.String == "Full" then
          st = n..": "..t
        end
        table.insert(text,st)
      end
    end

    for _,v in pairs(text) do
      if statusText~="" then statusText = statusText..", " end
      statusText = statusText..v
    end

  end
  
  Status.Value = level
  Status.String = statusText
end

function updateDisplay(override)
  override = override or {Index=0}
  statusJson = {}
  local displayList = {}
  local indicate = table.clone(const.LEDs)
  local level = 0

  for i=1, const.Qty do
    s = Inputs[i].Value
    t = Inputs[i].String
    n = Labels[i].String

    if BtnDisable[i].Boolean then s = 0 end
    if BtnLatch[i].Boolean then if s<inputStates[i].Status then s=inputStates[i].Status end end
    if override.Index == i then
      s = override.Value
      t = override.String
    end

    if override.Index ~= i and BtnSuppress[i].Boolean and inputStates[i].Value ~= s then
      BtnSuppress[i].Boolean = false
    end

    level = s >= level and s or level
    if n == "" then n = "Input "..tostring(i) end
    
    inputStates[i] = {Name=n,Status=s,String=t}
    
    if Filter[s+1].Boolean then
      --"Some Name Here16  INITIALIZING"
      opt = string.sub(n,1,20)
      if string.len(opt)<=7 then opt = opt.."\t\t\t" elseif string.len(opt)<=14 then opt = opt.."\t\t" else opt = opt.."\t" end
      opt = opt ..const.StateText[s+1]
      table.insert(displayList,opt)
      table.insert(statusJson,{Name=n,Value=s,String=t})
    end

    if (s)>0 and (s)~=3 then
      indicate[1] = false
    end
    if (s)>0 then
      indicate[(s+1)] = true
    end
  end

  List.Choices = displayList
  setLED(indicate)
  setStatus(level)
  JSONOut.String = rapidjson.encode(statusJson)
end

-- Initialization Function
function Initialization()
  if #DetailLevel.Choices == 0 then
    DetailLevel.Choices = {"Label","Full","None"}
    for i=1,6 do Filter[i].Boolean = true end
    Filter[7].Boolean = true
  end
  if DetailLevel.String == "" then
    DetailLevel.String = "None"
  end

  List.Choices = {}
  Status.Value = const.States.OK

  --Init EventHandlers
  DetailLevel.EventHandler = updateDisplay
  for i=1,#Filter do Filter[i].EventHandler = function()filterSelect(i)end end

  for i=1, const.Qty do
    Inputs[i].EventHandler = function(evt)
      updateDisplay()
    end
    BtnDisable[i].EventHandler = function(evt)
      updateDisplay()
    end
    BtnSuppress[i].EventHandler = function(evt)
      if evt.Boolean then 
        updateDisplay({Index=i,Value=0,String=""})
      else
        updateDisplay()
      end
    end
    BtnLatch[i].EventHandler = function(evt)
      if evt.Boolean == false then 
        updateDisplay()
      end
    end
    BtnReset[i].EventHandler = function(evt)
      updateDisplay({Index=i,Value=Inputs[i].Value,String=Inputs[i].String})
    end
    Labels[i].EventHandler = function(evt)
      updateDisplay()
    end

    table.insert(inputStates,{Name=Labels[i].String,Status=Inputs[i].Value,String=Inputs[i].String})
  end

  updateDisplay()

end

Initialization()
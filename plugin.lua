
--[[ #include "info.lua" ]]

function GetColor(props)
  return {204, 204, 204}
end

function GetPrettyName(props)
  return "Better Status Combiner " .. PluginInfo.Version
end

function GetProperties()
  props = {}
  --[[ #include "properties.lua" ]]
  return props
end

function GetControls(props)
  ctrls = {}
  --[[ #include "controls.lua" ]]
  return ctrls
end

function GetControlLayout(props)
  layout   = {}
  graphics = {}
  --[[ #include "layout.lua" ]]

  zindex = 1
  for i=1,#graphics do
    graphics[i]["ZOrder"] = zindex
    zindex = zindex+1
  end
  for i=1,#layout do
    graphics[i]["ZOrder"] = zindex
    zindex = zindex+1
  end

  return layout, graphics
end

--Start event based logic
if Controls then
  --[[ #include "runtime.lua" ]]
end
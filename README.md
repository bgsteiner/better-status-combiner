# Better Status Combiner

This is a different take on the status combiner that I have found more useful when integrating it with UCI's or other scripts. The basic functions are the same as the built in but there is more configuration available.

## Additions

I have added a filter option that can be used to only show certain status events in a list or the provided JSON output.

You can select what the status Text will be
<None> Only show the actual status ie: (Ok, Compromised, Fault, ..etc)
<Label> Add the label for each input to that status message.
<Full> Same as the built in Status Combiner

Individual LEDs for each status level that will light up in any input is at that level.

There is a space for each status input that will show the actual status before the "Disable, Surpress, and Latch" buttons take effect.
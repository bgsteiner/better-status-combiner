--[[ Layout File Contents ]]
i_cont = props["Input Count"].Value

layout["Status"]  = {
  Style = "Text",
  PrettyName = "Combined Status",
  Position = {65,15},
  Size = {269,24},
  Color = {194, 194, 194},
  HTextAlign = "Center",
  TextBoxStyle = "Normal",
  StrokeWidth = 1,
  StrokeColor = {105, 105, 105},
  FontSize = 13,
  CornerRadius = 0,
  Margin = 0,
  Padding = 0,
  IsReadOnly = true,
  ZOrder = 1
}

for i=1,6 do
  text = {"OK","Compromised","Fault","Not Present","Missing","Initializing"}
  pos = {{20,60},{72,60},{126,60},{177,60},{237,60},{292,60}}
  color = {{0,159,60},{239,156,0},{223,0,36},{204,204,204},{255,0,0},{0,0,255}}
  layout["Leds "..tostring(i)] = {
    Style = "Led",
    PrettyName = "LED~"..text[i],
    Position = pos[i],
    Size = {24,24},
    Color = color[i],
    ZOrder = 1
  }
end

for i=1,6 do
  text = {"OK","Compromised","Fault","Not Present","Missing","Initializing"}
  pos = {i<=3 and (65+(75*(i-1))) or (65+(75*(i-4))), i<=3 and 104 or 124}
  layout["Filter "..tostring(i)] = {
      Style = "Button",
      ButtonStyle = "Toggle",
      Legend = text[i],
      Position = pos,
      Size = {75,20},
      Color = {255,255,255},
      Margin = 2,
      CornerRadius = 2,
      StrokeWidth = 1,
      ZOrder = 1
  }
end

layout["Filter 7"] = {
  Style = "Button",
  ButtonStyle = "Toggle",
  Legend = "Select All",
  Position = {65,149},
  Size = {75,20},
  Color = {255,255,255},
  Margin = 2,
  CornerRadius = 2,
  StrokeWidth = 1,
  ZOrder = 1
}
layout["Filter 8"] = {
  Style = "Button",
  ButtonStyle = "Toggle",
  Legend = "Select None",
  Position = {140,149},
  Size = {75,20},
  Color = {255,255,255},
  Margin = 2,
  CornerRadius = 2,
  StrokeWidth = 1,
  ZOrder = 1
}

layout["Detail"] = {
    Style = "ComboBox",
    TextBoxStyle = "Normal",
    Position = {140,174},
    Size = {150,16},
    Color= {255,255,255},
    Margin = 0,
    Padding = 0,
    StrokeWidth = 1,
    ZOrder = 1
}

layout["List"] = {
  Style = "ListBox",
  TextBoxStyle = "Normal",
  PrettyName = "Script~List",
  HTextAlign = "Left",
  Position = {343,15},
  Size = {180,200},
  Color= {198, 198, 198},
  Margin = 1,
  Padding = 2,
  StrokeWidth = 1,
  IsReadOnly = true,
  ZOrder = 1
}

layout["JSON"] = {
  Style = "ListBox",
  TextBoxStyle = "NoBackground",
  PrettyName = "Script~JSON",
  Position = {353,30},
  Margin = 10,
  Padding = 0,
  StrokeWidth = 0,
  IsReadOnly = true,
  ZOrder = 1
}

for i=1, i_cont do
  line = 248+(18*(i-1))

  layout["Disable"..tostring(i_cont == 1 and "" or " "..i)] = {
      Style = "Button",
      ButtonStyle = "Toggle",
      PrettyName = "Input "..i.."~Disable",
      Position = {56, line},
      Size = {40, 18},
      Color = {223,0,36},
      Margin = 2,
      CornerRadius = 2,
      StrokeWidth = 1,
      ZOrder = 160+i
  }
  layout["Suppress"..tostring(i_cont == 1 and "" or " "..i)] = {
      Style = "Button",
      ButtonStyle = "Toggle",
      PrettyName = "Input "..i.."~Suppress",
      Position = {106, line},
      Size = {40, 18},
      Color = {239,156,0},
      Margin = 2,
      CornerRadius = 2,
      StrokeWidth = 1,
      ZOrder = 290+i
  }
  layout["Latch"..tostring(i_cont == 1 and "" or " "..i)] = {
      Style = "Button",
      ButtonStyle = "Toggle",
      PrettyName = "Input "..i.."~Latch",
      Position = {156, line},
      Size = {40, 18},
      Color = {248,244,0},
      Margin = 2,
      CornerRadius = 2,
      StrokeWidth = 1,
      ZOrder = 420+i
  }
  layout["Reset"..tostring(i_cont == 1 and "" or " "..i)] = {
      Style = "Button",
      ButtonStyle = "Trigger",
      PrettyName = "Input "..i.."~Reset",
      Position = {206, line},
      Size = {40, 18},
      Color = {4,105,179},
      Margin = 2,
      CornerRadius = 2,
      StrokeWidth = 1,
      ZOrder = 550+i
  }
  layout["Label"..tostring(i_cont == 1 and "" or " "..i)] = {
      Style = "Text",
      TextBoxStyle = "Normal",
      PrettyName = "Input "..i.."~Label",
      HTextAlign = "Left",
      Position = {250, line},
      Size = {168,18},
      Margin = 1,
      Padding = 1,
      StrokeWidth = 1,
      ZOrder = 680+i
  }
  layout["Input"..tostring(i_cont == 1 and "" or " "..i)] = {
      Style = "Text",
      TextBoxStyle = "Normal",
      Position = {423, line},
      Size = {100,18},
      Color= {198, 198, 198},
      Margin = 1,
      Padding = 0,
      StrokeWidth = 1,
      IsReadOnly = true,
      ZOrder = 810+i
  }
end

table.insert(graphics,{
  Type = "GroupBox",
  StrokeColor = {0, 0, 0},
  StrokeWidth = 1,
  CornerRadius = 8,
  Position = {0, 0},
  Size = {543,276+(18*i_cont)},
  ZOrder = 1
})
table.insert(graphics,{
  Type = "GroupBox",
  Text = "Status",
  HTextAlign = "Left",
  StrokeColor = {0, 0, 0},
  StrokeWidth = 1,
  CornerRadius = 8,
  Position = {10,10},
  Size = {523,215},
  ZOrder = 1
})
table.insert(graphics,{
  Type = "GroupBox",
  StrokeColor = {0, 0, 0},
  StrokeWidth = 1,
  CornerRadius = 8,
  Position = {10,225},
  Size = {523,41+(18*i_cont)},
  ZOrder = 1
})

table.insert(graphics,{
  Type = "Label",
  Text = "OK",
  Position = {20,44},
  Size = {24,16},
  HTextAlign = "Center",
  FontSize = 11,
  ZOrder = 1
})
table.insert(graphics,{
  Type = "Label",
  Text = "Compromised",
  Position = {44,44},
  Size = {80,16},
  HTextAlign = "Center",
  FontSize = 11,
  ZOrder = 1
})
table.insert(graphics,{
  Type = "Label",
  Text = "Fault",
  Position = {124,44},
  Size = {30,16},
  HTextAlign = "Center",
  FontSize = 11,
  ZOrder = 1
})
table.insert(graphics,{
  Type = "Label",
  Text = "Not Present",
  Position = {154,44},
  Size = {70,16},
  HTextAlign = "Center",
  FontSize = 11,
  ZOrder = 1
})
table.insert(graphics,{
  Type = "Label",
  Text = "Missing",
  Position = {224,44},
  Size = {50,16},
  HTextAlign = "Center",
  FontSize = 11,
  ZOrder = 1
})
table.insert(graphics,{
  Type = "Label",
  Text = "Initializing",
  Position = {274,44},
  Size = {60,16},
  HTextAlign = "Center",
  FontSize = 11,
  ZOrder = 1
})
table.insert(graphics,{
  Type = "Label",
  Text = "Status Detail",
  Position = {65,174},
  Size = {75,16},
  HTextAlign = "Left",
  FontSize = 11,
  ZOrder = 1
})
table.insert(graphics,{
  Type = "Label",
  Text = "Created by Ben Steiner",
  Position = {10,205},
  Size = {135,15},
  HTextAlign = "Center",
  FontSize = 12,
  FontStyle = "Medium",
  ZOrder = 1
})

table.insert(graphics,{
  Type = "Header",
  Text = "Filter",
  Position = {65,89},
  Size = {225,10},
  HTextAlign = "Center",
  TextSize = 12,
})

table.insert(graphics,{
  Type = "Label",
  Text = "Input",
  Position = {15,230},
  Size = {35,18},
  HTextAlign = "Left",
  FontSize = 11,
  ZOrder = 1
})
table.insert(graphics,{
  Type = "Label",
  Text = "Disable",
  Position = {50,230},
  Size = {50,18},
  HTextAlign = "Center",
  FontSize = 11,
  ZOrder = 1
})
table.insert(graphics,{
  Type = "Label",
  Text = "Suppress",
  Position = {100,230},
  Size = {50,18},
  HTextAlign = "Center",
  FontSize = 11,
  ZOrder = 1
})
table.insert(graphics,{
  Type = "Label",
  Text = "Latch",
  Position = {150,230},
  Size = {50,18},
  HTextAlign = "Center",
  FontSize = 11,
  ZOrder = 1
})
table.insert(graphics,{
  Type = "Label",
  Text = "Reset",
  Position = {200,230},
  Size = {50,18},
  HTextAlign = "Center",
  FontSize = 11,
  ZOrder = 1
})
table.insert(graphics,{
  Type = "Label",
  Text = "Label",
  Position = {250,230},
  Size = {160,18},
  HTextAlign = "Left",
  FontSize = 11,
  ZOrder = 1
})

for i=1, i_cont do
  table.insert(graphics,{
      Type = "Label",
      Text = tostring(i),
      Position = {15,230+(18*i)},
      Size = {36, 18},
      HTextAlign = "Left",
      FontSize = 11,
      ZOrder = 1
  })
end
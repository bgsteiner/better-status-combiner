PluginInfo = {
    Name = "Ben Steiner~Better Status Combiner",
    Version = "1.1.0",
    BuildVersion = "1.1.0.0",
    Id = "8a276886-c732-b0af-0ae6-f067050ff3bc",
    Author = "Ben Steiner",
    Description = "A Better Status Combiner with more detail and breakout of each status level.",
    ShowDebug = false
}
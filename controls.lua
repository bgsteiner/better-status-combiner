table.insert(ctrls,
  {Name = "Status",ControlType = "Indicator",IndicatorType = "Status",PinStyle = "Output",UserPin = true,Count = 1}
)

table.insert(ctrls,
  {Name = "Leds",ControlType = "Indicator",IndicatorType = "Led",PinStyle = "Output",UserPin = true,Count = 6}
)

table.insert(ctrls,
  {Name = "Filter",ControlType = "Button",ButtonType = "Toggle",Count = 8}
)
table.insert(ctrls,
  {Name = "Detail",ControlType = "Text",Count = 1}
)

table.insert(ctrls,
  {Name = "List",ControlType = "Text",PinStyle = "Output",UserPin = true,Count = 1}
)
table.insert(ctrls,
  {Name = "JSON",ControlType = "Text",PinStyle = "Output",UserPin = true,Count = 1}
)

table.insert(ctrls,
  {Name = "Input",ControlType = "Indicator",IndicatorType = "Status",Count = props["Input Count"].Value,PinStyle = "Input",UserPin = false}
)

table.insert(ctrls,
  {Name = "Disable",ControlType = "Button",ButtonType = "Toggle",Count = props["Input Count"].Value,PinStyle = "Both",UserPin = true}
)
table.insert(ctrls,
  {Name = "Suppress",ControlType = "Button",ButtonType = "Toggle",Count = props["Input Count"].Value,PinStyle = "Input",UserPin = true}
)
table.insert(ctrls,
  {Name = "Latch",ControlType = "Button",ButtonType = "Toggle",Count = props["Input Count"].Value,PinStyle = "Input",UserPin = true}
)
table.insert(ctrls,
  {Name = "Reset",ControlType = "Button",ButtonType = "Toggle",Count = props["Input Count"].Value,PinStyle = "Input",UserPin = true}
)

table.insert(ctrls,
  {Name = "Label",ControlType = "Text",Count = props["Input Count"].Value,PinStyle = "Input",UserPin = true}
)


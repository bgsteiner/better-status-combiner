--[[ Properties File Contents ]]

table.insert(props,
  {
    Name = "Input Count",
    Type = "integer",
    Min  = 1,
    Max = 128,
    Value = 8
  }
)